'use strict'; // eslint-disable-line strict, lines-around-directive

const pagination = require('metalsmith-pagination');
const assignIn = require('lodash/assignIn');
const kebabCase = require('lodash/kebabCase');

const categoryPagination = function categoryPagination(config) {
    return (files, metalsmith, done) => {
        const metadata = metalsmith.metadata();
        const collectionName = config.collection.replace(/collections\./, '');
        let collectionFiles = metadata.collections[collectionName];
        collectionFiles = collectionFiles.filter(file => file.path.indexOf('index.html') === -1);
        metadata.paginationCategories = [];

        collectionFiles.forEach((file) => {
            file.category.forEach((category) => {
                if (metadata.paginationCategories.indexOf(category) === -1) {
                    metadata.paginationCategories.push(category);
                }
            });
        });
        const cb = function paginationCallback() {
            return true;
        };

        metadata.paginationCategories.forEach((category) => {
            pagination({
                [config.collection]: {
                    layout: config.layout || 'cmc-site.hbs',
                    path: 'blog/:name.html',
                    pageContents: config.pageContents,
                    key: category,
                    filter: file => !/index.html/.test(file.path) &&
                    file.category.indexOf(category) !== -1,

                    pageMetadata: assignIn(
                        {},
                        {
                            category,
                        },
                        config.addPageMetadata
                    ),

                    groupBy: (file, index, options) => {
                        if (!{}.hasOwnProperty.call(file, 'categoryKeys')) {
                            file.categoryKeys = {};
                        }
                        const page = Math.ceil((index + 1) / options.perPage);
                        const catString = kebabCase(category);
                        const key = `${catString}--${page}`;
                        file.categoryKeys[key] = category;
                        return key;
                    },
                },
            }).apply(this, [files, metalsmith, cb]);
        });

        setImmediate(done);
    };
};

module.exports = categoryPagination;
